# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      module JekyllBase
        # Overrides the default Link tag with it not usable for pdf, where we
        # don't want to have relative pathes.
        class LinkTag < Jekyll::Tags::Link

          def render(context)
            site = context.registers[:site]

            site.config["url"] + super
          end

        end
      end
      TempLiquid.register_tag("link", JekyllBase::LinkTag)
    end
  end
end
