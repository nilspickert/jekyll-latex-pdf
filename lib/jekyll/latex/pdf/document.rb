# frozen_string_literal: true

require "kramdown"
require "fileutils"

module Jekyll
  module Latex
    module Pdf
      # The document adds the file.
      class Document < Jekyll::Page

        attr_accessor :source

        def initialize(site, page)
          @site = site
          @page = page
          @base = site.source
          @dir = File.dirname(page.url)
          @name = File.basename(page.url, File.extname(page.url)) + ".pdf"
          @texname = File.basename(page.url, File.extname(page.url)) + ".tex"

          process(@name)

          self.data = page.data.clone
          self.content = page.content.clone
          self.source = page.content.dup
          page.data["pdf_url"] = url

          data["html_url"] = page.url
          data["date_str"] = data["date"].strftime("%Y-%m-%d")
          @config = Defaults.defaults.merge(site.config["pdf"] || {})

          @kramdowndata = KramdownData.new data
          @kramdowndata.add(@site.config["pdf"] || {})
          @kramdowndata.add(page.data || {})

          check_scholar
          check_figure
        end

        def check_scholar
          if @site.config["plugins"].include? "jekyll-scholar"

            require "jekyll/latex/pdf/scholar"

            @kramdowndata.add(bibtex_files: bibtex_files.map do |bibfile|
                                              File.join(@site.config["scholar"]["source"], bibfile)
                                            end)
          end
        end

        def check_figure
          if site.config["plugins"].include? "jekyll-figure"
            require "jekyll/latex/pdf/figure"
          end
        end

        def permalink
          data.key?("permalink") ? (data["permalink"] + ext) : nil
        end

        def write(dest)
          path = File.join(dest, CGI.unescape(url))

          latex = Latex.new(source, @site, @page, @kramdowndata, @name)
          if 0 == latex.compile
            # Create target directory if not existing yet (in case of a new post - the HTML rendering which creates folders if needed happens later, so we need to do it here first)
            FileUtils.mkdir_p(File.dirname(path.to_s))
            FileUtils.cp(latex.pdf_file, path)
            Jekyll.logger.debug "cp " + latex.pdf_file.to_s + " " + path.to_s
          end
        end

        def bibtex_files
          if @site.config["scholar"]['bibliography'].include? '*'
            @bibtex_files ||= Dir.glob(File.join(@site.config["scholar"]["source"],
                                                 @site.config["scholar"]["bibliography"])).
              collect do |f|
              Pathname(f).relative_path_from(Pathname(@site.config["scholar"]['source'])).to_s
            end
          end
          @bibtex_files ||= [@site.config["scholar"]['bibliography']]
        end

      end
    end
  end
end
