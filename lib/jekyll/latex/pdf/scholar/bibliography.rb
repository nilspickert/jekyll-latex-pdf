# frozen_string_literal: true

module Jekyll
  module Latex
    # This module generates pdf files using latex (must be installed on the system) and
    # should be highly configurable.
    module Pdf
      ## here we add support for jekyll-scholar, a great module for scientific writing.
      module Scholar
        # Overrides the jekyll-scholar bibliography tag to be used for latex generation.
        class Bibliography < Liquid::Tag

          include Utilities

          def initialize(tag_name, arguments, tokens)
            super
            @keys = arguments
          end

          def render(_context)
            nomarkdown "\\printbibliography"
          end

        end
      end

      TempLiquid.register_tag("bibliography", Scholar::Bibliography)
    end
  end
end
