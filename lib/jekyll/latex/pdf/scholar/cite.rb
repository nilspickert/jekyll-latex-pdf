# frozen_string_literal: true

module Jekyll
  module Latex
    # This module generates pdf files using latex (must be installed on the system) and
    # should be highly configurable.
    module Pdf
      # here we add support for jekyll-scholar, a great module for scientific writing.
      module Scholar
        # Overrides the jekyll-scholar cite tag to be used for generating latex documents.
        class Cite < Liquid::Tag

          include Utilities
          def initialize(tag_name, arguments, tokens)
            super

            @keys = arguments
          end

          def render(_context)
            # set_context_to context+
            if @keys.kind_of? String
              nomarkdown "\\parencite\{#{@keys.strip}\}"
            else
              nomarkdown "\\parencite\{" + @keys.collect {|x| x.strip || x }.join(",") + "\}"
            end
          end

        end
      end

      TempLiquid.register_tag("cite", Scholar::Cite)
    end
  end
end
