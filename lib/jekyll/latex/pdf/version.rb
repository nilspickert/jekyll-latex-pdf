# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      VERSION = "0.5.4"
    end
  end
end
