# frozen_string_literal: true

require "jekyll/latex/pdf/scholar/cite"
require "jekyll/latex/pdf/scholar/bibliography"
