# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      # Add methods which can be useful anywhere in the package.
      module Utilities
        # Surround a string with nomarkdown tags to tell kramdown that this is
        # plain latex.
        def nomarkdown(string_to_wrap)
          "{::nomarkdown type=\"latex\"}" + string_to_wrap + "{:/}"
        end

        def nomarkdown_p(string_to_wrap)
          "\n{::nomarkdown type=\"latex\"}\n" + string_to_wrap + "\n{:/}\n"
        end

        # Run some commands und stop on errors.
        def run_cmds(cmds, tempdir)
          status = 0
          out = "\n"
          cmds.each do |cmd|
            Open3.popen2(*cmd, chdir: tempdir) do |_i, oe, t|
              oe.each {|l| out += l + "\n" }

              status = t.value
            end
            unless 0 == status
              Jekyll.logger.warn "jekyll-latex-pdf", "Error running #{cmd.join ' '}"
              Jekyll.logger.warn "Return status: #{status}"
              Jekyll.logger.warn out
            end
          end
          [out, status]
        end

        # stolen from jekyll-scholar
        def set_context_to(context)
          @context, @site, = context, context.registers[:site]
          config.merge!(site.config['scholar'] || {})
          self
        end
      end
    end
  end
end
