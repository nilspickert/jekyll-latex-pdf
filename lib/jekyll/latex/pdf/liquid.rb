# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      # Generates an environment where we temporarily can add new tags to the templet,
      # without changing the registered tags.
      class TempLiquid

        def saved_liquid_tags
          @saved_liquid_tags ||= {}
        end

        def register_tag(tag_key, tag_value)
          saved_liquid_tags[tag_key] = Liquid::Template.tags[tag_key]
          Liquid::Template.register_tag(tag_key, tag_value)
        end

        def register_tags
          TempLiquid.tags.each do |tag_key, tag_value|
            register_tag(tag_key, tag_value)
            Jekyll.logger.debug "Register tag:", tag_key.to_s
          end
        end

        def tmp_keys
          @tmp_keys ||= []
        end

        def restore_liquid_tags
          saved_liquid_tags.each do |tag_key, tag_value|
            Liquid::Template.register_tag tag_key, tag_value
          end
        end

        def self.run
          s = TempLiquid.new
          s.register_tags
          yield s
          s.restore_liquid_tags
        end

        def self.tags
          @@liquid_tags_register || {}
        end

        def self.register_tag(name, klass)
          @@liquid_tags_register ||= {}
          @@liquid_tags_register[name.to_s] = klass
        end

      end
    end
  end
end
