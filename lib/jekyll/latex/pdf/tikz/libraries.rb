# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      module Tikz
        class TikzLibraries

          def self.add_lib(lib)
            @@libs ||= []
            @@libs << lib unless libs.include?(lib)
          end

          def self.libs
            @@libs ||= []
            @@libs.sort
          end

          def self.render
            libs.empty? ? "" : "\\usetikzlibrary{#{libs.join(',')}}\n"
          end

        end
      end
    end
  end
end
