# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      module Tikz
        module TikzUtils
          def parse_markup(markup)
            h = markup.split " "

            @file_name = h.shift
            #            @params ||= {}

            k = ""
            h.each do |v|
              if v.end_with? ":"
                k = v[0..-2]
                next
              end
              TikzLibraries.add_lib(v) if "library" == k
            end
          end
        end
      end
    end
  end
end
