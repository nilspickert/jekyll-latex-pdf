# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf
      module Tikz
        class TikzPdf < Liquid::Block

          include Utilities
          include TikzUtils

          def initialize(tag_name, markup, tokens)
            super

            parse_markup(markup)

            @header = <<~'END'
              \begin{tikzpicture}
            END

            @footer = <<~'END'
              \end{tikzpicture}
            END
          end

          def render(context)
            nomarkdown_p(@header + super + @footer)
          end

        end
      end

      TempLiquid.register_tag("tikz", Tikz::TikzPdf)
    end
  end
end
