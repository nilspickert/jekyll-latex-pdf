# frozen_string_literal: true
# Authored my Maxfan http://github.com/Maxfan-zone http://maxfan.org
# Adjusted to the need of jekyll-latex-pdf by Martin Kaffanke
# This is used to convert tikz code into svg file and load in you jekyll site
#
# Install:
#
#   1. Copy this file in your _plugins/ directory. You can customize it, of course.
#   2. Make sure texlive and pdf2svg are installed on your computer.
#   3. Set path to pdf2svg in _config.yml in "pdf2svg" variable
#
# Input:
#
#   {% tikz filename %}
#     \tikz code goes here
#   {% endtikz %}
#
# This will generate a /img/post-title-from-filename/filename.svg in your jekyll directory
#
# And then return this in your HTML output file:
#
#   <embed src="/img/post-title-from-filename/tikz-filename.svg" type="image/svg+xml" />
#

require "open3"

module Jekyll
  module Latex
    module Pdf
      module Tikz
        class TikzHtml < Liquid::Block

          include Utilities
          include TikzUtils

          attr_reader :config, :site

          def initialize(tag_name, markup, tokens)
            super
            parse_markup(markup)

            @header = <<-'END'
        \documentclass{standalone}
        \usepackage{tikz}
            END

            # @header += "\\usetikzlibrary{#{@params['library']}}\n" if @params.key?("library")
            @header += TikzLibraries.render

            @header += <<-'END'
        \begin{document}
        \begin{tikzpicture}
            END

            @footer = <<-'END'
        \end{tikzpicture}
        \end{document}
            END

            @config = Defaults.defaults.dup
          end

          def render(context)
            set_context_to context

            tikz_code = @header + super + @footer
            md5, old_md5 = false, false

            tmp_directory = File.join(Dir.pwd,
                                      @site.config["pdf"]["latex_cache_path"],
                                      "tikz",
                                      File.basename(context["page"]["url"], ".*"))
            FileUtils.mkdir_p tmp_directory

            dest_directory = File.join(Dir.pwd, @site.config["pdf"]["tikz_path"], File.basename(context["page"]["url"], ".*"))
            FileUtils.mkdir_p dest_directory

            # use new filename if content has changed.
            if @site.config["pdf"]["tikz_md5"]
              # add it to file name.
              md5 = Digest::MD5.hexdigest(tikz_code).to_s

              md5_filename = File.join(tmp_directory, "md5.txt")

              old_md5 = File.open(md5_filename, 'r').read if File.exist? md5_filename
              old_filename = @file_name + "_#{old_md5}"

              unless old_md5.eql? md5
                FileUtils.rm_rf(Dir.glob(File.join(tmp_directory, "*")))
                svg_path = File.join(dest_directory, "#{old_filename}.svg")
                png_path = File.join(dest_directory, "#{old_filename}.png")
                FileUtils.rm(svg_path) if File.exist? svg_path
                FileUtils.rm(png_path) if File.exist? png_path
              end

              @file_name += "_#{md5}"

              File.open(md5_filename, 'w') {|file| file.write(md5.to_s) }
            end

            tex_path = File.join(tmp_directory, "#{@file_name}.tex")
            pdf_path = File.join(tmp_directory, "#{@file_name}.pdf")

            svg_path = File.join(dest_directory, "#{@file_name}.svg")
            png_path = File.join(dest_directory, "#{@file_name}.png")

            # if the file doesn't exist or the tikz code is not the same with the file, then compile the file
            if !File.exist?(tex_path) || !tikz_same?(tex_path, tikz_code) ||
                !File.exist?(svg_path) || !File.exist?(png_path)
              File.open(tex_path, 'w') {|file| file.write(tikz_code.to_s) }
              cmds = [[@site.config["pdf"]["pdf_engine"],
                       "--interaction=batchmode",
                       "--output-format=pdf", tex_path],
                      ["pdf2svg", pdf_path, svg_path],
                      ["gm", "convert", svg_path, "-quality", "90", png_path]]

              run_cmds cmds, tmp_directory
            end

            web_svg_path = File.join(@site.config["pdf"]["tikz_path"],
                                     File.basename(context["page"]["url"], ".*"),
                                     "#{@file_name}.svg")
            web_png_path = File.join(@site.config["pdf"]["tikz_path"],
                                     File.basename(context["page"]["url"], ".*"),
                                     "#{@file_name}.png")
            "\n<object data=\"/#{web_svg_path}\" type=\"image/svg+xml\">" \
              "<img src=\"/#{web_png_path}\" />" \
              "</object>\n"
          end

          private

          def tikz_same?(file, code)
            File.open(file, 'r') do |tikzfile|
              tikzfile.read == code
            end
          end

        end

        Liquid::Template.register_tag('tikz', TikzHtml)
      end
    end
  end
end
