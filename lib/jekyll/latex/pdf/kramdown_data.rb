# frozen_string_literal: true

module Jekyll
  module Latex
    module Pdf

      # Hold data for kramdown renderer.  We have some extra things added to the
      # default post content.
      class KramdownData

        def initialize(*args)
          add(*args)
        end

        def add(*args)
          @data ||= {}
          @data.merge! hash_args(*args)
        end

        attr_reader :data

        private

        def hash_args(*args)
          hashed_args = args.detect {|f| f.class == Hash }
          hashed_args.each_with_object({}) {|(k, v), memo| memo[k.to_sym] = v; }
        end

      end
    end
  end
end
