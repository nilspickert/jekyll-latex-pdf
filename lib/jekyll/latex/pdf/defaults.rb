# frozen_string_literal: true

module Jekyll
  module Latex
    # Default values for configuration.
    module Pdf
      class Defaults

        # Defaults
        @defaults = {

          # Engine to render latex to pdf.
          "pdf_engine" => "lualatex",

          # Default binary to handle bibtex files.  Using biblatex for default.
          "bib_engine" => "biber",

          # Default template to use
          "template" => "jekyll-latex-pdf",

          # Template path under site
          "template_path" => "_latex",

          # Use the date in the \note command instead of the \date.  Usefull for apa6
          "date_as_note" => false,

          # Set the path where the rendered svg files should be stored.
          "tikz_path" => "assets/tikz",

          # Directory where the latex files will be keept.
          "latex_cache_path" => ".latex-cache",

          # Using md5 sum for tikz svg and png files.  Useful if you edit your
          # files and want the browser to reload the object.
          "tikz_md5" => false,
        }
        class << self

          def prepare_defaults
            @defaults["default_template_path"] = File.expand_path(File.join(File.dirname(__FILE__),
                                                                            "..", "..", "..", "..",
                                                                            "data", "kramdown"))
          end

          def defaults
            prepare_defaults
            @defaults
          end

        end

      end
    end
  end
end
