# frozen_string_literal: true
module Jekyll
  module Latex
    module Pdf
      module Figure
        # Overrides the figref tag from jekyll-figure
        class FigRefTag < Liquid::Tag

          include Utilities

          def initialize(tag_name, markup, tokens)
            @label = markup.gsub(/\s/, '')
            super
          end

          def render(_context)
            nomarkdown "\\ref{#{@label}}"
          end

        end
      end
      TempLiquid.register_tag("figref", Figure::FigRefTag)
    end
  end
end
